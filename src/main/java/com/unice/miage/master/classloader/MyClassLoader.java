package com.unice.miage.master.classloader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class MyClassLoader extends SecureClassLoader {

	private ArrayList<File> path = new ArrayList<File>();

	public void addPath(File file) {
		path.add(file);
	}
	
	/**
	 * 
	 * @param name
	 * @return
	 * @throws ClassNotFoundException
	 */
	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {

		byte[] b = loadClassData(name);
		return super.defineClass(name, b, 0, b.length);
	}

	/**
	 * 
	 * @param name
	 * @return
	 * @throws ClassNotFoundException
	 */
	private byte[] loadClassData(String name) throws ClassNotFoundException {
		
		String fileName = name.replace(".", File.separator) + ".class";
		byte[] bytes = null;
		for (File file : path) {
			Path classFilePath = null;
			if (file.isDirectory()) {
				classFilePath = Paths.get(file.getAbsolutePath(), fileName);
			} else if (file.getName().endsWith(".jar") || file.getName().endsWith(".zip")) {
				try {
					File fileJar = Paths.get(file.getAbsolutePath()).toFile();
					ZipFile zipFile = new ZipFile(fileJar);
					Enumeration<? extends ZipEntry> entries = zipFile.entries();
					ZipEntry entry = null;
					while(entries.hasMoreElements()){
						entry = entries.nextElement();
						String entryName = entry.getName();						
						if(new File(entryName).equals(new File(fileName))){
							break;
						}
					}
					
					if(entry == null){
						continue;
					}
					
					InputStream in = zipFile.getInputStream(entry);
					ByteArrayOutputStream byteArray = new ByteArrayOutputStream();

					int r = -1;
					while ((r = in.read()) != -1) {
						byteArray.write(r);
					}

					in.close();
					
					zipFile.close();
					return byteArray.toByteArray();

				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				classFilePath = Paths.get(file.getAbsolutePath());
				if (!classFilePath.endsWith(Paths.get(fileName))) {
					continue;
				}
			}
			
			if(!classFilePath.toFile().exists()){
				continue;
			}

			try {
				bytes = Files.readAllBytes(classFilePath);
				break;
			} catch (IOException e) {
				System.err.println("Class not found in : " + file.getAbsolutePath());
			}
		}

		if (bytes == null) {
			throw new ClassNotFoundException();
		}

		return bytes;
	}
}
