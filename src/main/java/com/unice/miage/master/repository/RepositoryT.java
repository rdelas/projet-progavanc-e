package com.unice.miage.master.repository;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.unice.miage.master.classloader.MyClassLoader;

public class RepositoryT<T> {

	private File base;

	private MyClassLoader cl;
	
	private Class<T> classT;
	
	private List<Class<? extends T>> classes;

	public RepositoryT(File base, Class<T> superClass) {
		this.base = base;
		cl = new MyClassLoader();
		cl.addPath(base);
		this.classT = superClass;
		classes = new ArrayList<Class<? extends T>>();
	}

	/**
	 * 
	 * @return
	 */
	public List<Class<? extends T>> load() {
		

		List<File> files = listFilteredFolderRecursive(base, ".*\\.(class|jar)");

		for (File file : files) {
			if(file.getName().endsWith(".jar")){
				cl.addPath(file);
				JarFile jar = null;
				try {
					jar = new JarFile(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
				loadClassFromJar(jar);
			} else {
				loadClassFromFile(file);
			}
			
		}

		return classes;
	}

	/**
	 * 
	 * @param jar
	 */
	@SuppressWarnings("unchecked")
	private void loadClassFromJar(JarFile jar){
		Enumeration<JarEntry> entries = jar.entries();
		while(entries.hasMoreElements()){
			String entryName = entries.nextElement().getName();
			
			if(entryName.endsWith(".class")){
			
				Path path = Paths.get(entryName);
				String substring = path.toString().substring(0, path.toString().lastIndexOf("."));
				String name = substring.replaceAll("[/|\\\\]", ".");
				try {
					Class<?> c = Class.forName(name, true, cl);
					List<Class<?>> superClasses = getAllSuperclasses(c);
					List<Class<?>> interfaces = getAllInterfaces(c);
					if(superClasses.contains(classT) || interfaces.contains(classT)){
						classes.add((Class<? extends T>) c);
					}
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * 
	 * @param file
	 */
	@SuppressWarnings("unchecked")
	private void loadClassFromFile(File file){
		Path path = Paths.get(file.getPath());
		path = Paths.get(base.getPath()).relativize(path);
		String substring = path.toString().substring(0, path.toString().lastIndexOf("."));
		String name = substring.replaceAll("[/|\\\\]", ".");
		try {
			Class<?> c = Class.forName(name, true, cl);
			List<Class<?>> superClasses = getAllSuperclasses(c);
			List<Class<?>> interfaces = getAllInterfaces(c);
			if(superClasses.contains(classT) || interfaces.contains(classT)){
				classes.add((Class<? extends T>) c);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param c
	 * @return
	 */
	private List<Class<?>> getAllSuperclasses(Class<?> c) {
		
		if(c.equals(Object.class)){
			return new ArrayList<Class<?>>();
		}
		
		List<Class<?>> superclasses = new ArrayList<Class<?>>();
		
		superclasses.addAll(getAllSuperclasses(c.getSuperclass()));
		superclasses.add(c.getSuperclass());
		
		return superclasses;
	}
	
	/**
	 * 
	 * @param c
	 * @return
	 */
	private List<Class<?>> getAllInterfaces(Class<?> c) {
		
		if(c.equals(Object.class))
			return new ArrayList<Class<?>>();
		
		List<Class<?>> interfaces = new ArrayList<Class<?>>();
		
		interfaces.addAll(getAllInterfaces(c.getSuperclass()));
		interfaces.addAll(Arrays.asList(c.getInterfaces()));
		
		return interfaces;
	}

	/**
	 * 
	 * @param file
	 * @param filter
	 * @return
	 */
	private static List<File> listFilteredFolderRecursive(File file, String filter) {
		List<File> files = new ArrayList<File>();

		FilenameFilter filterObj = new FilenameFilterRecursive(filter);
		File[] listFiles = file.listFiles(filterObj);

		for (File subFile : listFiles) {

			if (subFile.isDirectory()) {
				files.addAll(listFilteredFolderRecursive(subFile, filter));
			} else {
				files.add(subFile);
			}
		}

		return files;
	}

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		RepositoryT<SecureClassLoader> rep = new RepositoryT<SecureClassLoader>(new File("bin/com/unice/miage/master/tp3"), SecureClassLoader.class);
		List<Class<? extends SecureClassLoader>> classes = rep.load();
		
		for (Class<?> class1 : classes) {
			System.out.println(class1.getName());
		}
		
	}

	static class FilenameFilterRecursive implements FilenameFilter {

		private String regex;

		public FilenameFilterRecursive(String regex) {
			this.regex = regex;
		}

		public boolean accept(File dir, String name) {
			File f = new File(dir.getPath() + "/" + name);
			if (f.isDirectory()) {
				return true;
			}

			Pattern p = Pattern.compile(regex);

			Matcher m = p.matcher(name);
			return m.matches();
		}

	}
}
