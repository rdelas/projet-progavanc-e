package com.unice.miage.master.repository;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.unice.miage.master.classloader.MyClassLoader;

public class Repository {
	
	private File base;
	
	private MyClassLoader cl;
	
	/**
	 * 
	 * @param base
	 */
	public Repository(File base) {
		this.base = base;
		cl = new MyClassLoader();
		cl.addPath(base);
	}

	/**
	 * 
	 * @return
	 */
	public List<Class<?>> load() {
		List<Class<?>> classes = new ArrayList<Class<?>>();

		List<File> files = listFilteredFolderRecursive(base, ".*\\.class");

		for (File file : files) {

			Path path = Paths.get(file.getPath());
			path = Paths.get(base.getPath()).relativize(path);
			String substring = path.toString().substring(0, path.toString().lastIndexOf("."));
			String name = substring.replaceAll("[/|\\\\]", ".");

			try {
				classes.add(Class.forName(name, true, cl));
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return classes;
	}

	/**
	 * 
	 * @param file
	 * @param filter
	 * @return
	 */
	private static List<File> listFilteredFolderRecursive(File file, String filter) {
		List<File> files = new ArrayList<File>();

		FilenameFilter filterObj = new FilenameFilterRecursive(filter);
		File[] listFiles = file.listFiles(filterObj);

		for (File subFile : listFiles) {

			if (subFile.isDirectory()) {
				files.addAll(listFilteredFolderRecursive(subFile, filter));
			} else {
				files.add(subFile);
			}
		}
		return files;
	}

	static class FilenameFilterRecursive implements FilenameFilter {

		private String regex;

		public FilenameFilterRecursive(String regex) {
			this.regex = regex;
		}

		public boolean accept(File dir, String name) {
			File f = new File(dir.getPath() + File.separator + name);
			if (f.isDirectory()) {
				return true;
			}

			Pattern p = Pattern.compile(regex);

			Matcher m = p.matcher(name);
			return m.matches();
		}

	}
}
