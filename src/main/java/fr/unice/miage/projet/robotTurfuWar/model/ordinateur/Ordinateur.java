package fr.unice.miage.projet.robotTurfuWar.model.ordinateur;

import java.util.Map;

import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;

public class Ordinateur {

	private Tank tank;
	private IAttackBehavior attack;
	private IMoveBehavior move;
	private IDefenseBehavior defense;

	/**
	 * 
	 * @param tank
	 * @param behaviors
	 */
	public Ordinateur(Tank tank, Map<EBehaviors, IBehavior> behaviors) {
		this.tank = tank;
		this.attack = (IAttackBehavior) behaviors.get(EBehaviors.ATTACK);
		this.move = (IMoveBehavior) behaviors.get(EBehaviors.MOVE);
		this.defense = (IDefenseBehavior) behaviors.get(EBehaviors.DEFENSE);
	}

	public void attack() {
		attack.action(tank);
	}

	public void move() {
		move.action(tank);
	}
	
	/**
	 * 
	 * @param i
	 */
	public void defense(int i){
		defense.action(tank,i);
	}

	/**
	 * 
	 * @param attack
	 */
	public void setAttackBehavior(IAttackBehavior attack) {
		this.attack = attack;
	}

	/**
	 * 
	 * @param move
	 */
	public void setMoveBehavior(IMoveBehavior move) {
		this.move = move;
	}
	
	/**
	 * 
	 * @param defense
	 */
	public void setDefense(IDefenseBehavior defense) {
		this.defense = defense;
	}

	public enum EBehaviors {
		ATTACK,
		DEFENSE,
		MOVE;
	}
}
