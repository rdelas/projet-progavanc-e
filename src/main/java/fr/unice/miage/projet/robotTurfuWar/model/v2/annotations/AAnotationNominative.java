package fr.unice.miage.projet.robotTurfuWar.model.v2.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface AAnotationNominative {
	String[] nom();
}
