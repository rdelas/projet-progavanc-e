package fr.unice.miage.projet.robotTurfuWar.ihm.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "plugins")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PluginListConfiguration {
	private List<PluginConfiguration> pluginss;
	
	/**
	 * 
	 * @return
	 */
	@XmlElement(name = "plugin")
	public List<PluginConfiguration> getPlugins() {
		return pluginss;
	}

	/**
	 * 
	 * @param plugins
	 */
	public void setPlugins(List<PluginConfiguration> plugins) {
		this.pluginss = plugins;
	}
}