package fr.unice.miage.projet.robotTurfuWar.view.launcher;

import java.io.InputStream;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Launcher extends Application {
	private static Stage stageMain;
	
	@Override
	public void start(Stage stage) throws Exception {
		stageMain = stage;
		
		InputStream input = ClassLoader.getSystemResourceAsStream("xml/starter.fxml");
		Parent root = new FXMLLoader().load(input);
        Scene scene = new Scene(root);
        AnchorPane main = (AnchorPane) scene.lookup("#mainPane");
        ImageView iv = (ImageView) scene.lookup("#background-image");
        
        iv.fitHeightProperty().bind(main.heightProperty());
        iv.fitWidthProperty().bind(main.widthProperty());
        stageMain.setTitle("Robot War Turfu");
        stageMain.setScene(scene);
        stageMain.centerOnScreen();
        stageMain.show();
	}

	public static void main(String[] args) {
		launch(args);
	}


	
//	public class ResourceFiles {
//		public static Image getImage(String name) throws IOException {	
//			return ImageIO.read(ClassLoader.getSystemResourceAsStream("img/" + name));
//		}
//	}
//	
	/**
	 * 
	 * @return
	 */
	public static Stage getStageMain() {
		return stageMain;
	}
}
