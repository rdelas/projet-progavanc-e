package fr.unice.miage.projet.robotTurfuWar.model.v2.composant;

import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.LinkedList;

import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.plugin.BatterieBase;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.plugin.CanonFreezer;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.plugin.CanonGrosRecul;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.plugin.GenerateurBase;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.plugin.PropulsionBase;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.plugin.PropulsionCos;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.plugin.PropulsionUnidirectionel;

public class UsineSerie {
	
	private static final String BASE = "xml"+File.separator+"composants"+File.separator;
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		CanonFreezer a = new CanonFreezer("piou piou", 10, 100, 20, 1000,50 , 25);
		CanonFreezer b = new CanonFreezer("short canon",25,100,50,2,12,75);
		CanonGrosRecul g = new CanonGrosRecul("bloum",25,100,50,2,12,75);
	
		Batterie l  = new BatterieBase("nuclear", 40, 100, 1, 800);
		
		Generateur k = new GenerateurBase("fioul", 10, 400,10,25,3000);
		Propulsion p = new PropulsionBase("chenille", 100, 100,25,5);
		Propulsion cos = new PropulsionCos("CosLife",10000,100,3,6);
		Propulsion n = new PropulsionUnidirectionel("Straffer", 100, 540, 12, 6);

		LinkedList<IComposant> list = new LinkedList<>();
		list.add(a);
		list.add(b);
		list.add(g);
		list.add(l);
		list.add(k);
		list.add(p);
		list.add(n);
		list.add(cos);
		try {
			for(IComposant ar : list){
				System.out.println(ar.getClass().getSuperclass().getSimpleName());
				XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream( new FileOutputStream(BASE+ar.getNom()+"."+ar.getClass().getSuperclass().getSimpleName())));
				encoder.writeObject(ar);
				encoder.flush();
				encoder.close();
			}
		} catch (final java.io.IOException e) {
			e.printStackTrace();
		}
		System.out.println("done");
	}

}
