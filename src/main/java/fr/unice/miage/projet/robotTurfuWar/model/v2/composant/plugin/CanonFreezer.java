package fr.unice.miage.projet.robotTurfuWar.model.v2.composant.plugin;

import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;
import fr.unice.miage.projet.robotTurfuWar.model.v2.annotations.AAnotationNominative;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Arme;

public class CanonFreezer extends Arme{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7529108730481391128L;

	public CanonFreezer() {
	}
	
	/**
	 * 
	 * @param nom
	 * @param pv
	 * @param poids
	 * @param consommation
	 * @param delaisTir
	 * @param degat
	 * @param portee
	 */
	@AAnotationNominative(nom = {"nom","pv","poids","consommation","delaisTir","degat","portee"})
	public CanonFreezer(String nom, int pv, double poids, int consommation,int delaisTir, int degat, int portee) {
		super(nom, pv, poids, consommation, delaisTir, degat, portee);
	}

	@Override
	public void tirer(Tank me,Tank... cible) {
		cible[0].recevoirDegat(this.getDegat());
		cible[0].getPropulsion().setDeplacement(cible[0].getPropulsion().getDeplacement()*0.8);
		
	}
	
	
}
