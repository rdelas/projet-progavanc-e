package fr.unice.miage.projet.robotTurfuWar.model.v2;

public interface TankObservable {
	public void ajouterObservateur(TankObservateur o);
	public void notifierObservateurs();
}
