package fr.unice.miage.projet.robotTurfuWar.model.v2.composant.plugin;

import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;
import fr.unice.miage.projet.robotTurfuWar.model.v2.annotations.AAnotationNominative;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Arme;

public class CanonGrosRecul extends Arme{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7760320097293493052L;

	public CanonGrosRecul() {
	}
	/**
	 * 
	 * @param nom
	 * @param pv
	 * @param poids
	 * @param consommation
	 * @param delaisTir
	 * @param degat
	 * @param portee
	 */
	@AAnotationNominative(nom = {"nom","pv","poids","consommation","delaisTir","degat","portee"})
	public CanonGrosRecul(String nom, int pv, double poids, int consommation,int delaisTir, int degat, int portee) {
		super(nom, pv, poids, consommation, delaisTir, degat, portee);
	}

	@Override
	public void tirer(Tank me,Tank... cible) {
		if(this.getPv()>=0){
			cible[0].recevoirDegat(this.getDegat());
			me.getPropulsion().setPv(me.getPropulsion().getPv() - 15);
		}
	}

}
