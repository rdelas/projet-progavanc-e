package fr.unice.miage.projet.robotTurfuWar.ihm.model;

import javax.xml.bind.annotation.XmlTransient;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PluginConfiguration {
	private final StringProperty name;
	private final BooleanProperty active;
	private final Package packageClass;
	private final EPlugin type;
		
	public PluginConfiguration() {
		this(null, false, null, null);
	}
	
	/**
	 * 
	 * @param name
	 * @param active
	 * @param packageClass
	 * @param type
	 */
	public PluginConfiguration(String name, boolean active, Package packageClass, EPlugin type) {
		this.name = new SimpleStringProperty(name);
	    this.active = new SimpleBooleanProperty(active);
	    this.packageClass = packageClass;
	    this.type = type;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getName() {
		return this.name.get();
	}
	
	public void setName(String name) {
		this.name.set(name);
	}
	
	/**
	 * 
	 * @return
	 */
	public Boolean getActive() {
		return this.active.get();
	}
	
	/**
	 * 
	 * @return
	 */
	public Package getPackage() {
		return this.packageClass;
	}
	
	/**
	 * 
	 * @return
	 */
	public EPlugin getType() {
		return this.type;
	}
	
	/**
	 * 
	 * @param active
	 */
	@XmlTransient
	public void setActive(Boolean active) {
		this.active.set(active);
	}
	
	/**
	 * 
	 * @return
	 */
	public StringProperty nameProperty() {
		return this.name;
	}

	/**
	 * 
	 * @return
	 */
	public BooleanProperty activeProperty() {
		return this.active;
	}
	
	public enum EPlugin {
		ORDINATEUR("ordi"),
		TANK("tank"),
		GRAPHIQUE("ihm");
		
		private String value;
		
		private EPlugin(String value) {
			this.value = value;
		}
		
		public String getValue() {
			return value;
		}
	}
}