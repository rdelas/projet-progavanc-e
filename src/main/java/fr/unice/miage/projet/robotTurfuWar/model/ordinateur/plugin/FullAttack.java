package fr.unice.miage.projet.robotTurfuWar.model.ordinateur.plugin;

import java.util.List;

import fr.unice.miage.projet.robotTurfuWar.model.ChampDeTank;
import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.IAttackBehavior;
import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;

public class FullAttack implements IAttackBehavior {

	public void action(Tank me) {
		ChampDeTank champ = ChampDeTank.getChamp();
		List<Tank> list = champ.getTanks();
		Tank plusProche = null;
		for(Tank t : list){
			if(t.equals(me)){
				continue;
			}
			if(t.getPV()>0){
				if(plusProche==null){
					plusProche=t;
				}
				if(t.getPosition().distance(me.getPosition())< plusProche.getPosition().distance(me.getPosition())){
					plusProche=t;
				}
			}
		}
		if(plusProche!=null){
			int portee = me.getArme().getPortee();

			if(portee>= me.getPosition().distance(plusProche.getPosition())){

			me.attaquerCible(plusProche);
			}
			System.out.println("La cible : "+plusProche.getName() + " hors de portee");
		}
		else {
			Pacifiste p = new Pacifiste();
			me.getOrdi().setAttackBehavior(p);
		}
	}

}
