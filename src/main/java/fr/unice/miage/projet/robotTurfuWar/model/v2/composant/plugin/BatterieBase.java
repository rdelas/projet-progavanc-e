package fr.unice.miage.projet.robotTurfuWar.model.v2.composant.plugin;

import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;
import fr.unice.miage.projet.robotTurfuWar.model.v2.annotations.AAnotationNominative;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Batterie;

public class BatterieBase extends Batterie{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2009328535881897355L;

	/**
	 * 
	 * @param nom
	 * @param pv
	 * @param poids
	 * @param consommation
	 * @param stamina
	 */
	@AAnotationNominative(nom = {"nom","pv","poids","consommation","stamina"})	
	public BatterieBase(String nom, int pv, double poids, int consommation, int stamina) {
		super(nom, pv, poids, consommation, stamina);
		System.out.println("kikoooooooo"+stamina);
	}
	
	public BatterieBase() {
	}
	
	@Override
	public void consommer(int value,Tank me,Tank...tanks) {
		this.setStamina(this.getStamina() - value);
	}

	@Override
	public void recharger(int value,Tank me) {
		System.out.println(this.getStamina());
		this.setStamina(this.getStamina() + value);
	}

}
