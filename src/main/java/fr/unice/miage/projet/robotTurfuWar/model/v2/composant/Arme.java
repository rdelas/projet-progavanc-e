package fr.unice.miage.projet.robotTurfuWar.model.v2.composant;

import java.io.Serializable;
import java.util.Date;

import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;


public abstract class Arme implements IComposant, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6080198238984496451L;
	protected String nom;
	protected int pv;
	protected double poids;
	protected int consommation;
	protected int delaisTir;
	protected int degat;
	protected Date dateLastShot;
	protected int portee;
	
	public Arme(){}
		
	/**
	 * 
	 * @param nom
	 * @param pv
	 * @param poids
	 * @param consommation
	 * @param delaisTir
	 * @param degat
	 * @param portee
	 */
	public Arme(String nom, int pv, double poids, int consommation,int delaisTir, int degat, int portee) {
		this.nom = nom;
		this.pv=pv;
		this.poids = poids;
		this.consommation=consommation;
		this.delaisTir=delaisTir;
		this.degat = degat;
		this.dateLastShot = new Date();
		this.portee = portee;
	}
	
	@Override
	public String getNom() {
		return nom;
	}

	@Override
	public void setNom(String nom) {
		this.nom=nom;
	}

	@Override
	public int getPv() {
		return pv;
	}

	@Override
	public void setPv(int pv) {
		this.pv=pv;

	}

	@Override
	public double getPoids() {
		return this.poids;
	}

	@Override
	public void setPoids(double poids) {
		this.poids=poids;
	}

	/**
	 * 
	 * @param me
	 * @param cible
	 */
	public abstract void tirer(Tank me ,Tank... cible);

	@Override
	public int getConsommation() {
		return this.consommation;
	}

	@Override
	public void setConsommation(int conso) {
		this.consommation = conso;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getDelaisTir() {
		return delaisTir;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getDegat() {
		return this.degat;
	}
	/**
	 * 
	 * @param i
	 */
	public void setDegat(int i) {
		this.degat=i;	
	}
	/**
	 * 
	 * @param d
	 */
	public void setDateLastShot(Date d) {
		this.dateLastShot = d;
	}
	
	/**
	 * 
	 * @return
	 */
	public Date getDateLastShot() {
		return dateLastShot;
	}
	public int getPortee() {
		return portee;
	}

	/**
	 * 
	 * @param i
	 */
	public void setDelaisTir(int i) {
		this.delaisTir=i;
	}

	/**
	 * 
	 * @param i
	 */
	public void setPortee(int i) {
		this.portee=i;
	}

}
