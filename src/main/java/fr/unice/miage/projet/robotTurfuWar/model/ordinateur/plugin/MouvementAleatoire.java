package fr.unice.miage.projet.robotTurfuWar.model.ordinateur.plugin;

import java.util.Random;

import fr.unice.miage.projet.robotTurfuWar.model.ChampDeTank;
import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.IMoveBehavior;
import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;
import javafx.geometry.Point2D;

public class MouvementAleatoire implements IMoveBehavior{

	private int loop = 0;
	private Point2D p;
	
	@Override
	public void action(Tank me) {
		ChampDeTank champ = ChampDeTank.getChamp();
		Tank nearest = getNearestTank(me, champ.getTanks());
		if(loop == 0 || (nearest!=null && nearest.contains(p))){
			Random rnd = new Random();
			Point2D temp = null;
			do{
				double x = rnd.nextDouble()*champ.getWidth();
				double y = rnd.nextDouble()*champ.getHeight();
						
				p = new Point2D(x, y);
				temp = me.simulerDeplacement(p);
			}while(nearest != null && nearest.contains(temp));
			
		}
		loop = (loop+1)%25;
		me.deplacer(p);
	}

}
