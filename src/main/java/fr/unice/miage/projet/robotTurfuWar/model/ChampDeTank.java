package fr.unice.miage.projet.robotTurfuWar.model;

import java.beans.ExceptionListener;
import java.beans.XMLDecoder;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.IBehavior;
import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.Ordinateur.EBehaviors;
import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;
import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank.EComposants;
import fr.unice.miage.projet.robotTurfuWar.model.v2.TankConfig;
import fr.unice.miage.projet.robotTurfuWar.model.v2.TankConfigurations;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.IComposant;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

public class ChampDeTank extends Canvas {

	private static ChampDeTank champ;
	private List<Tank> tanks;
	private Thread t;
	private volatile boolean shutdown;

	/**
	 * 
	 * @return
	 */
	public static ChampDeTank getChamp() {
		if (champ == null) {
			champ = new ChampDeTank();
		}
		return champ;
	}

	private ChampDeTank() {
		super();
	}

	public void play() {
		this.shutdown = true;
		this.t = new Thread(new Runnable() {

			@Override
			public void run() {
				while (shutdown) {
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					for (Tank t : tanks) {
						if (t.getPV() <= 0) {
							
						} else {
							t.move();
							t.attack();
							t.recharger();
						}
					}
					drawTanks();
				}
			}
		});
		t.setDaemon(true);
		t.start();
	}
	
	
	public void stop() {
		this.shutdown = false;
	}
	
	public void buildTanks() {
		List<TankConfig> tanksConfig = TankConfigurations.getInstance().getConfigs();
		tanks = new ArrayList<Tank>();
		Random rnd = new Random();
		for (TankConfig tankConfig : tanksConfig) {
			System.out.println(tankConfig.getNom());
			System.out.println(this.getWidth());
			Point2D p = new Point2D(rnd.nextDouble() * this.getWidth(), rnd.nextDouble() * this.getHeight());

			tanks.add(createTank(p, tankConfig));
		}
	}

	/**
	 * 
	 * @param position
	 * @param tankConfig
	 * @return
	 */
	private Tank createTank(Point2D position, TankConfig tankConfig) {
		Map<EComposants, IComposant> composants = new HashMap<EComposants, IComposant>();
		Map<EBehaviors, IBehavior> behaviors = new HashMap<EBehaviors, IBehavior>();

		for (EComposants key : EComposants.values()) {
			String c = tankConfig.getComponent(key);
			composants.put(key, (IComposant) loadComposant(c));
		}

		for (EBehaviors key : EBehaviors.values()) {
			Class<? extends IBehavior> c = tankConfig.getBehavior(key);
			IBehavior behavior = null;
			try {
				behavior = c.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
			if (behavior != null) {
				behaviors.put(key, behavior);
			}
		}

		Tank t = new Tank(tankConfig.getNom(), position, tankConfig.getCouleur(), composants, behaviors);
		return t;

	}

	/**
	 * 
	 * @param nom
	 * @return
	 */
	private Object loadComposant(String nom) {
		XMLDecoder decoder = null;
		try {
			decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(nom)));
			decoder.setExceptionListener(new ExceptionListener() {
				public void exceptionThrown(final Exception ex) {
					ex.printStackTrace();
				}
			});
		} catch (final Exception e) {
			e.printStackTrace();
		}
		Object o = null;
		if (decoder != null) {
			o = decoder.readObject();
		}
		return o;
	}

	
	private void drawTanks() {
		GraphicsContext gc = this.getGraphicsContext2D();
		gc.clearRect(0, 0, this.getWidth(), this.getHeight());
		for (Tank t : tanks) {
			if(t.getPV() > 0){
				t.drawTank(gc);
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	public List<Tank> getTanks() {
		return tanks;
	}

}
