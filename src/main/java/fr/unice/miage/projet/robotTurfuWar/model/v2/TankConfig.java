package fr.unice.miage.projet.robotTurfuWar.model.v2;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.IBehavior;
import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.Ordinateur.EBehaviors;
import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank.EComposants;
import javafx.scene.paint.Color;

public class TankConfig implements Serializable{

	private static final long serialVersionUID = 15543433526658393L;
	private String nom;
	private Color couleur;
	private Map<EComposants, String> composants;
	private Map<EBehaviors, Class<? extends IBehavior>>  behaviors;
	private TankConfig(){
		composants = new HashMap<>(4);
		behaviors = new HashMap<>(3);
	}
	
	/**
	 * 
	 * @param nom
	 * @param couleur
	 */
	public TankConfig(String nom, Color couleur){
		this();
		this.nom = nom;
		this.couleur = couleur;
	}
	
	/**
	 * 
	 * @param key
	 * @param value
	 */
	public void addComponent(EComposants key, String value){
		composants.put(key, value);
	}

	/**
	 * 
	 * @param key
	 * @param value
	 */
	public void addBehavior(EBehaviors key, Class<? extends IBehavior> value){
		behaviors.put(key, value);
	}
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	public String getComponent(EComposants key){
		return composants.get(key);
	}
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	public Class<? extends IBehavior> getBehavior(EBehaviors key){
		return behaviors.get(key);
	}
	/**
	 * 
	 * @return
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * 
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * 
	 * @return
	 */
	public Color getCouleur() {
		return couleur;
	}

	/**
	 * 
	 * @param couleur
	 */
	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}

}
