package fr.unice.miage.projet.robotTurfuWar.model.plugin;

import java.io.File;
import java.util.List;

import com.unice.miage.master.repository.RepositoryT;

import fr.unice.miage.projet.robotTurfuWar.ihm.model.IInfosTank;

public class IhmPlugins {

	private static IhmPlugins instance;
	
	private static final File BASE = new File("plugin"+File.separator+"ihm");
	
	private List<Class<? extends IInfosTank>> ihmPlugins;
	
	
	private IhmPlugins(){
		loadInfosTank();
	}
	
	/**
	 * 
	 * @return
	 */
	public static IhmPlugins getInstance(){
		if(instance == null){
			instance = new IhmPlugins();
		}
		
		return instance;
	}
	
	public static void reload() {
		instance.loadInfosTank();
	}
	
	private void loadInfosTank() {
		RepositoryT<IInfosTank> rInfosTank = new RepositoryT<IInfosTank>(BASE, IInfosTank.class);
		this.ihmPlugins = rInfosTank.load();
	}

	/**
	 * 
	 * @return
	 */
	public List<Class<? extends IInfosTank>> getInfosTank() {
		return this.ihmPlugins;
	}
}
