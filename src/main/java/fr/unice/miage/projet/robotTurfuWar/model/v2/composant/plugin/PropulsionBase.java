package fr.unice.miage.projet.robotTurfuWar.model.v2.composant.plugin;

import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;
import fr.unice.miage.projet.robotTurfuWar.model.v2.annotations.AAnotationNominative;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Propulsion;
import javafx.geometry.Point2D;

public class PropulsionBase extends Propulsion {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2867647972992551714L;

	public PropulsionBase() {
	}
	
	/**
	 * 
	 * @param nom
	 * @param pv
	 * @param poids
	 * @param consommation
	 * @param deplacement
	 */
	@AAnotationNominative(nom = {"nom","pv","poids","consommation","deplacement"})
	public PropulsionBase(String nom, int pv, double poids, int consommation, double deplacement) {
		super(nom, pv, poids, consommation, deplacement);
	}

	@Override
	public Point2D deplacer(Point2D maPos, Point2D posCible,Tank me) {
		double deplacementX = posCible.getX() -maPos.getX() ;
		double deplacementY = posCible.getY() -maPos.getY() ;
		double deplacementParIterationX = (this.deplacement/2);
		double deplacementParIterationY = (this.deplacement/2);
		double xFin = maPos.getX();
		double yFin = maPos.getY();

		if(deplacementX !=0 || deplacementY !=0){

			while ( deplacementParIterationX!=0 || deplacementParIterationY!=0 ){
				if(deplacementX==0){
					if(deplacementY!=0){
						deplacementParIterationY+=deplacementParIterationX;
						deplacementParIterationX=0;
					}
				}
				if(deplacementY==0){
					if(deplacementX!=0){
						deplacementParIterationX+=deplacementParIterationY;
						deplacementParIterationY=0;
					}
				}
				if(deplacementParIterationX !=0){
					if(deplacementParIterationX>=Math.abs(deplacementX)){
						xFin+=deplacementX;
						deplacementParIterationX-=deplacementX;
						if(deplacementY!=0){
							deplacementParIterationY+=deplacementParIterationX;
						}
						deplacementParIterationX=0;
						deplacementX=0;
					}
					else if(deplacementParIterationX<Math.abs(deplacementX)){
						xFin+=(deplacementParIterationX)*(deplacementX/Math.abs(deplacementX));
						deplacementX-=deplacementParIterationX;
						deplacementParIterationX=0;
						deplacementX=0;
					}
				}
				if(deplacementParIterationY!=0){
					if(deplacementParIterationY>=Math.abs(deplacementY)){
						yFin+=deplacementY;
						deplacementParIterationY-=deplacementY;
						deplacementY=0;
						if(deplacementX!=0){
							deplacementParIterationX+=deplacementParIterationY;
						}
						deplacementParIterationY=0;
					}
					else if(deplacementParIterationY < Math.abs(deplacementY) ){
						yFin+=(deplacementParIterationY)*(deplacementY/Math.abs(deplacementY));
						deplacementY-=deplacementParIterationY;
						deplacementParIterationY=0;
						deplacementY=0;
					}
				}
			}
		}
		return new Point2D(xFin, yFin);
	}

}
