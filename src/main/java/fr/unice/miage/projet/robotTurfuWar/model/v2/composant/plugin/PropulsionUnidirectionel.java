package fr.unice.miage.projet.robotTurfuWar.model.v2.composant.plugin;

import java.util.Random;

import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;
import fr.unice.miage.projet.robotTurfuWar.model.v2.annotations.AAnotationNominative;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Propulsion;
import javafx.geometry.Point2D;

public class PropulsionUnidirectionel extends Propulsion {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4914850987857677829L;

	public PropulsionUnidirectionel() {
	}
	/**
	 * 
	 * @param nom
	 * @param pv
	 * @param poids
	 * @param consommation
	 * @param deplacement
	 */
	@AAnotationNominative(nom = {"nom","pv","poids","consommation","deplacement"})
	public PropulsionUnidirectionel(String nom, int pv, double poids, int consommation, double deplacement) {
		super(nom, pv, poids, consommation, deplacement);
	}

	@Override
	public Point2D deplacer(Point2D maPos, Point2D posCible,Tank me) {
		double x = maPos.getX();
		double y = maPos.getY();
		double deplacementX =posCible.getX() -maPos.getX();
		double deplacementY = posCible.getY() -maPos.getY();
		Random r = new Random();
		Boolean hor = r.nextBoolean();
		if(hor){
			if(Math.abs(deplacementX) > this.getDeplacement()){
				double gn = this.getDeplacement() * (Math.abs(deplacementX)/deplacementX);
				x+=gn;
			}
			else{
				double k = deplacementX * (Math.abs(deplacementX) / deplacementX);
				x+=k;
			}
		}
		else{
			if(Math.abs(deplacementY) > this.getDeplacement()){
				double gn = this.getDeplacement() * (Math.abs(deplacementY)/deplacementY);
				y+=gn ; 
			}
			else{
				double k = deplacementY * (Math.abs(deplacementY)/deplacementY);
				y+=k;
			}
		}
		return new Point2D(x, y);
	}
}