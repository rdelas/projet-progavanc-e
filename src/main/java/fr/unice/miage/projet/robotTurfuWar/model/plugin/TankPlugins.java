package fr.unice.miage.projet.robotTurfuWar.model.plugin;

import java.io.File;
import java.util.List;

import com.unice.miage.master.repository.RepositoryT;

import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Arme;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Batterie;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Generateur;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Propulsion;

public class TankPlugins {

	private static TankPlugins instance;
	private static final File BASE = new File("plugin"+File.separator+"tank"+File.separator);
	private List<Class<? extends Arme>> armes;
	private List<Class<? extends Batterie>> batteries;
	private List<Class<? extends Generateur>> generateurs;
	private List<Class<? extends Propulsion>> propulsions;
	private TankPlugins(){
		loadArmes();
		loadBatteries();
		loadGenerateurs();
		loadPropulsion();
	}
	
	/**
	 * 
	 * @return
	 */
	public static TankPlugins getInstance(){
		if(instance == null){
			instance = new TankPlugins();
		}
		return instance;
	}
	
	public static void reload() {
		instance.loadArmes();
		instance.loadBatteries();
		instance.loadGenerateurs();
		instance.loadPropulsion();
	}
	
	private void loadArmes() {
		RepositoryT<Arme> rArmes = new RepositoryT<Arme>(BASE, Arme.class);
		armes = rArmes.load();
	}
	
	private void loadBatteries(){
		RepositoryT<Batterie> rBatteries = new RepositoryT<Batterie>(BASE, Batterie.class);
		batteries = rBatteries.load();
	}

	private void loadGenerateurs(){
		RepositoryT<Generateur> rGenerateurs = new RepositoryT<Generateur>(BASE, Generateur.class);
		generateurs = rGenerateurs.load();
	}
	
	private void loadPropulsion(){
		RepositoryT<Propulsion> rPropulsions = new RepositoryT<Propulsion>(BASE, Propulsion.class);
		propulsions = rPropulsions.load();

	}

	/**
	 * @return the armes
	 */
	public List<Class<? extends Arme>> getArmes() {
		return armes;
	}

	/**
	 * @return the batteries
	 */
	public List<Class<? extends Batterie>> getBatteries() {
		return batteries;
	}

	/**
	 * @return the generateurs
	 */
	public List<Class<? extends Generateur>> getGenerateurs() {
		return generateurs;
	}

	/**
	 * @return the propulsions
	 */
	public List<Class<? extends Propulsion>> getPropulsions() {
		return propulsions;
	}
	
}
