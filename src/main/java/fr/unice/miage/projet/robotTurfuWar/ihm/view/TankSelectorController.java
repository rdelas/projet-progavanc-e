package fr.unice.miage.projet.robotTurfuWar.ihm.view;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import fr.unice.miage.projet.robotTurfuWar.ihm.model.IInfosTank;
import fr.unice.miage.projet.robotTurfuWar.model.ChampDeTank;
import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.IBehavior;
import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.Ordinateur.EBehaviors;
import fr.unice.miage.projet.robotTurfuWar.model.plugin.IhmPlugins;
import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank.EComposants;
import fr.unice.miage.projet.robotTurfuWar.model.v2.TankConfig;
import fr.unice.miage.projet.robotTurfuWar.model.v2.TankConfigurations;
import fr.unice.miage.projet.robotTurfuWar.view.launcher.Launcher;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class TankSelectorController {	
	private List<TankGeneratorIHM> list;

	private VBox vb;

	private Button addTankButton = new Button("Ajouter un tank");

	private Button deleteTankButton = new Button("Supprimer un tank");

	private Button playButton = new Button("Jouer");

	public void start() throws Exception {
		list = new ArrayList<TankGeneratorIHM>();
		BorderPane root = new BorderPane();
		ScrollPane sp = new ScrollPane();
		vb = new VBox();
		vb.setPadding(new Insets(5));
		vb.setSpacing(10);
		addTank();
		addTank();

		sp.setContent(vb);

		root.setCenter(sp);

		HBox hbBottom = new HBox();
		hbBottom.setPadding(new Insets(15));
		hbBottom.setSpacing(10);

		deleteTankButton.setDisable(true);
		addTankButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				addTank();
			}
		});
		deleteTankButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				ObservableList<Node> children = vb.getChildren();
				list.remove(children.remove(children.size() - 1));
				if (children.size() == 2) {
					deleteTankButton.setDisable(true);
				}
			}
		});

		playButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				try {
					buildGameAndPlay();
					InputStream input = ClassLoader.getSystemResourceAsStream("xml/game.fxml");
					FXMLLoader fxmlLoader = new FXMLLoader();
					Parent root1 = (Parent) fxmlLoader.load(input);

					Launcher.getStageMain().setScene(new Scene(root1));
					Launcher.getStageMain().setResizable(true);
					Launcher.getStageMain().sizeToScene();
					Launcher.getStageMain().setTitle("Robot War Turfu");
					Launcher.getStageMain().centerOnScreen();
					Launcher.getStageMain().show();
					
					ChampDeTank champ = ChampDeTank.getChamp();
					AnchorPane zoneChamp = (AnchorPane) Launcher.getStageMain().getScene().lookup("#champ");
					Pane zoneInfo = (Pane) Launcher.getStageMain().getScene().lookup("#infoBloc");
					champ.setWidth(zoneChamp.getWidth());
					champ.setHeight(zoneChamp.getHeight());
					zoneChamp.getChildren().add(champ);
					champ.buildTanks();
					
					List<Class<? extends IInfosTank>> i = IhmPlugins.getInstance().getInfosTank();
					IInfosTank info = i.get(i.size()-1).newInstance();
					info.getInfosTank(zoneInfo);
					
					champ.play();
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		hbBottom.getChildren().addAll(addTankButton, deleteTankButton, playButton);

		root.setBottom(hbBottom);

		Scene scene = new Scene(root);
		
		Launcher.getStageMain().setTitle("Configuration des tanks : Robot War Turfu");
		Launcher.getStageMain().setScene(scene);
		Launcher.getStageMain().centerOnScreen();

		Launcher.getStageMain().setResizable(false);
		Launcher.getStageMain().setHeight(600);
		Launcher.getStageMain().setWidth(800);
		Launcher.getStageMain().show();
	}

	private void addTank() {
		try {
			Random rnd = new Random();
			InputStream input = ClassLoader.getSystemResourceAsStream("xml/TankConfig.fxml");
			TankGeneratorIHM box = new FXMLLoader().load(input);
			box.loadAll();
			((TextField) box.lookup("#nameField")).setText("IA" + (vb.getChildren().size() + 1));
			((ColorPicker) box.lookup("#colorPicker")).setValue(Color.rgb(rnd.nextInt(255), rnd.nextInt(255), rnd.nextInt(255)));
			list.add(box);
			vb.getChildren().add(box);
			deleteTankButton.setDisable(false);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private void buildGameAndPlay() {
		TankConfigurations.getInstance().resetConfig();
		
		for (TankGeneratorIHM tank : list) {
			String name = ((TextField) tank.lookup("#nameField")).getText();
			Color color = ((ColorPicker) tank.lookup("#colorPicker")).getValue();

			String a = ((ChoiceBox<File>) tank.lookup("#armeChoice")).getValue().getAbsolutePath();
			String b = ((ChoiceBox<File>) tank.lookup("#batterieChoice")).getValue().getAbsolutePath();
			String c = ((ChoiceBox<File>) tank.lookup("#generateurChoice")).getValue().getAbsolutePath();
			String d = ((ChoiceBox<File>) tank.lookup("#propulsionChoice")).getValue().getAbsolutePath();

			Class<? extends IBehavior> e = ((ChoiceBox<Class<? extends IBehavior>>) tank.lookup("#attackChoice"))
					.getValue();
			Class<? extends IBehavior> f = ((ChoiceBox<Class<? extends IBehavior>>) tank.lookup("#moveChoice"))
					.getValue();
			Class<? extends IBehavior> g = ((ChoiceBox<Class<? extends IBehavior>>) tank.lookup("#defenseChoice"))
					.getValue();

			TankConfig config = new TankConfig(name, color);
			config.addComponent(EComposants.ARME, a);
			config.addComponent(EComposants.BATTERIE, b);
			config.addComponent(EComposants.GENERATEUR, c);
			config.addComponent(EComposants.PROPULTION, d);

			config.addBehavior(EBehaviors.ATTACK, e);
			config.addBehavior(EBehaviors.MOVE, f);
			config.addBehavior(EBehaviors.DEFENSE, g);

			TankConfigurations.getInstance().addConfig(config);
		}
	}
}
