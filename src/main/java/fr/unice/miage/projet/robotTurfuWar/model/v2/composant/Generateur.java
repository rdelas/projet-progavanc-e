package fr.unice.miage.projet.robotTurfuWar.model.v2.composant;

import java.io.Serializable;

import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;
import fr.unice.miage.projet.robotTurfuWar.model.v2.annotations.AAnotationNominative;

public abstract class Generateur implements IComposant,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3007111383798782370L;
	protected String nom;
	protected int pv;
	protected double poids;
	protected int consommation;
	protected int production;
	protected int ressource;
	
	public Generateur() {
	}
	/**
	 * 
	 * @param nom
	 * @param pv
	 * @param poids
	 * @param consommation
	 * @param production
	 * @param ressource
	 */
	@AAnotationNominative(nom = {"nom","pv","poids","consommation","production","ressource"})
	public Generateur(String nom, int pv, double poids, int consommation, int production,int ressource) {
		this.nom = nom;
		this.pv = pv;
		this.poids = poids;
		this.consommation = consommation;
		this.production=production;
		this.ressource=ressource;
	}

	@Override
	public String getNom() {
		return nom;
	}

	@Override
	public void setNom(String nom) {
		this.nom=nom;
	}

	@Override
	public int getPv() {
		return pv;
	}

	@Override
	public void setPv(int pv) {
		this.pv=pv;
	}

	@Override
	public double getPoids() {
		return this.poids;
	}

	@Override
	public void setPoids(double poids) {
		this.poids=poids;
	}

	@Override
	public int getConsommation() {
		return this.consommation;
	}

	@Override
	public void setConsommation(int conso) {
		this.consommation = conso;
	}

	/**
	 * 
	 * @param me
	 * @return
	 */
	public abstract int produire(Tank me);

	public  abstract void consommerRessource();

	/**
	 * 
	 * @param i
	 */
	public void setRessource(int i) {
		this.ressource = i;
	}

	/**
	 * 
	 * @return
	 */
	public int getRessource() {
		return this.ressource;
	}

	/**
	 * 
	 * @param i
	 */
	public void setProduction(int i) {
		this.production = i;
	}

	/**
	 * 
	 * @return
	 */
	public int getProduction() {
		return this.production;
	}

}
