package fr.unice.miage.projet.robotTurfuWar.generateurComposant;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.unice.miage.master.repository.Repository;

import fr.unice.miage.projet.robotTurfuWar.model.v2.annotations.AAnotationNominative;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Arme;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Batterie;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Generateur;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.IComposant;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Propulsion;

public class GenerateurComposant extends JFrame{

	private static final long serialVersionUID = -1193931132271580479L;
	private LinkedList<JTextField> listInfo; 
	private JPanel center;
	private JFileChooser dialogue ;
	private JComboBox<String> jCom;
	@SuppressWarnings("rawtypes")
	private final DefaultComboBoxModel model;
	private HashMap<String, Class<?>> listeClass;
	private Vector<?> comboBoxItems;
	private Class<?> classActu;
	private JMenuBar menuBarre;
	private JButton charger;
	private JButton sav;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public GenerateurComposant() {
		super("Generateur de composants");
		listInfo = new LinkedList<>();
		center=new JPanel();
		center.setLayout(new GridLayout(0,2));
		listeClass = new HashMap<>();
		comboBoxItems =new Vector();
		model = new DefaultComboBoxModel(comboBoxItems);
		jCom = new JComboBox<>(model);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(400, 420);
		this.setPreferredSize(new Dimension(200, 200));

		menuBarre = new JMenuBar();

		charger = new JButton("Charger");
		charger.addActionListener(new comboLis());

		sav = new JButton("Sauvegarde");
		sav.addActionListener(new savG());

		JButton ouvrir = new JButton("Ouvrir fichier plugin");

		ouvrir.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println(e.getActionCommand());
				dialogue= new JFileChooser();
				dialogue.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

				int status = dialogue.showOpenDialog(null);
				if (status == JFileChooser.APPROVE_OPTION) {
					chargeElem();

				} else if (status == JFileChooser.CANCEL_OPTION) {
				}
			}

		});
		menuBarre.add(ouvrir);
		menuBarre.add(jCom);
		menuBarre.add(charger);
		menuBarre.add(sav);

		this.getContentPane().add(center);
		this.add(menuBarre,BorderLayout.NORTH);
		this.setVisible(true);
		System.out.println(menuBarre.getSize());
		this.setSize(new Dimension((int) menuBarre.getSize().getWidth(),440));
	}
	
	@SuppressWarnings("unchecked")
	public void chargeElem(){

		File selectedFile = dialogue.getSelectedFile();
		
		Repository r = new Repository(selectedFile);
		List<Class<?>> list = r.load();
		model.removeAllElements();
		listeClass = new HashMap<>();
		for(Class<?> c : list){
			if(c!=Arme.class && c!=Batterie.class &&
					c!= Generateur.class &&
					c!= Propulsion.class){

				System.out.println(c.getName());
				model.addElement(c.getSimpleName().toString());
				listeClass.put(c.getSimpleName(), c);
			}
		}
		System.out.println(jCom.getSize());
		int tailleX = (int)(menuBarre.getSize().getWidth()+jCom.getSize().getWidth()+
				charger.getSize().getWidth() + sav.getWidth());
		menuBarre.repaint();
		this.setSize(new Dimension(tailleX,440));
	}
	
	public class savG implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			Constructor<?>[] c = classActu.getConstructors();
			Constructor<?> consTemp =null;
			for (int i = 0; i < c.length; i++) {
				if(c[i].getParameterCount()!=0){
					consTemp = c[i];
				}
			}
			Class<?>[] param_types = new Class<?>[consTemp.getParameterCount()];
			Object[] arguments = new Object[consTemp.getParameterCount()];
			Class<?>[] paramT = consTemp.getParameterTypes();
			for (int i = 0; i < paramT.length; i++) {
				String objTemp = listInfo.get(i).getText();
				System.out.println(objTemp);
				System.out.println(paramT[i].getTypeName());
				System.out.println(paramT[i].getName());

				if(paramT[i].getTypeName() == "int"){
					System.err.println("kiko");
					param_types[i] = Integer.TYPE;
					arguments[i]= Integer.parseInt((String) objTemp);
				}

				else if(paramT[i].getName() == "int"){
					System.err.println("kiko");
					param_types[i] = Integer.TYPE;
					arguments[i]= Integer.parseInt((String) objTemp);
				}
				else if(paramT[i].getName() =="double"){
					param_types[i] = double.class;

					arguments[i] = Double.parseDouble(objTemp);
				}
				else{
					param_types[i] = String.class;

					arguments[i] = objTemp;
				}

			}

			Constructor<?> couuu = null;
			Object rs =null;
			try {
				couuu = classActu.getConstructor(param_types);

				rs = couuu.newInstance(arguments);
			} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
				e1.printStackTrace();
			}
			System.out.println("String plus int constructor sample: "+rs);
			XMLEncoder encoder = null;
			JFileChooser jFilou = new JFileChooser();

			jFilou.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			jFilou.showOpenDialog(null);
			try {
				encoder = new XMLEncoder(new BufferedOutputStream( new FileOutputStream(jFilou.getSelectedFile().getAbsolutePath()+"//"+((IComposant) rs).getNom()+"."+rs.getClass().getSuperclass().getSimpleName())));
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
			encoder.writeObject(rs);
			encoder.flush();
			encoder.close();
			JOptionPane.showMessageDialog(null, ((IComposant)rs).getNom()+" sauvegarder a l'adresse : " +jFilou.getSelectedFile().getAbsolutePath()+"//"+((IComposant) rs).getNom()+"."+rs.getClass().getSuperclass().getSimpleName() );
		}
	}
	public class comboLis implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			if(jCom.getSelectedItem()!=null){
				center.removeAll();
				listInfo = new LinkedList<>();

				classActu = listeClass.get(jCom.getSelectedItem());
				Constructor<?>[] cons = classActu.getDeclaredConstructors();
				for (int i = 0; i < cons.length; i++) {
					Class<?>[] listParam = cons[i].getParameterTypes();
					if(listParam.length!=0){
						AAnotationNominative ano =(AAnotationNominative) cons[i].getAnnotation(AAnotationNominative.class);
						System.err.println(ano.toString());
						String[] nom = ano.nom();

						for (int j = 0; j < nom.length; j++) {
							System.err.println(nom[j].toString());
							JLabel joo = new JLabel(nom[j].toString());
							JTextField babbaaa = new JTextField();//nom[j]);
							listInfo.add(babbaaa);
							center.add(joo,j,0);
							center.add(babbaaa,j,1);
						}
					}
				}
				center.revalidate();
				center.repaint();
			}
		}

	}
	public static void main(String[] args) {
		new GenerateurComposant();
	}
}
