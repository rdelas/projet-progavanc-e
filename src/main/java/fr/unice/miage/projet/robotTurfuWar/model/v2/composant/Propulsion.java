package fr.unice.miage.projet.robotTurfuWar.model.v2.composant;

import java.io.Serializable;

import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;
import javafx.geometry.Point2D;

public abstract class Propulsion implements IComposant, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 137574949140520693L;
	protected String nom;
	protected int pv;
	protected double poids;
	protected int consommation;
	protected double deplacement;


	public Propulsion() {
	}

	/**
	 * 
	 * @param nom
	 * @param pv
	 * @param poids
	 * @param consommation
	 * @param deplacement
	 */
	public Propulsion(String nom, int pv, double poids, int consommation,double deplacement) {
		super();
		this.nom = nom;
		this.pv = pv;
		this.poids = poids;
		this.consommation = consommation;
		this.deplacement = deplacement;
	}

	@Override
	public String getNom() {
		return nom;
	}

	@Override
	public void setNom(String nom) {
		this.nom=nom;
	}

	@Override
	public int getPv() {
		return pv;
	}

	@Override
	public void setPv(int pv) {
		this.pv=pv;
	}

	@Override
	public double getPoids() {
		return this.poids;
	}

	@Override
	public void setPoids(double poids) {
		this.poids=poids;
	}

	@Override
	public int getConsommation() {
		return this.consommation;
	}

	@Override
	public void setConsommation(int conso) {
		this.consommation = conso;
	}
	
	/**
	 * 
	 * @return
	 */
	public Double getDeplacement() {
		return this.deplacement;
	}
	
	/**
	 * 
	 * @param i
	 */
	public void setDeplacement(Double i) {
		this.deplacement = i;
	}
	
	/**
	 * 
	 * @param maPos
	 * @param posCible
	 * @param me
	 * @return
	 */
	public abstract Point2D deplacer(Point2D maPos,Point2D posCible,Tank me);

}
