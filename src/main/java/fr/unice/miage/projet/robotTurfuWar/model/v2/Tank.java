package fr.unice.miage.projet.robotTurfuWar.model.v2;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.IBehavior;
import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.Ordinateur;
import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.Ordinateur.EBehaviors;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Arme;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Batterie;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Generateur;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.IComposant;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Propulsion;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.StrokeLineJoin;

public class Tank extends Circle implements TankObservable {
	private String name;
	private Ordinateur ordi;
	private Point2D position;
	private Color couleur;
	private Map<EComposants, IComposant> composants;
	private Tank target = null;
	private int ttl = 0;
	private ArrayList<TankObservateur> tabObservateur;

	/**
	 * 
	 * @param name
	 * @param position
	 * @param couleur
	 * @param composants
	 * @param behaviors
	 */
	public Tank(String name, Point2D position, Color couleur, Map<EComposants, IComposant> composants, Map<EBehaviors, IBehavior> behaviors) {
		super(position.getX(), position.getY(), 15, couleur);
		this.name = name;
		setPosition(position);
		this.couleur = couleur;
		this.setOrdi(new Ordinateur(this, behaviors));
		this.setComposants(composants);
		this.tabObservateur = new ArrayList<TankObservateur>();
	}

	public void attack(){
		System.err.println("jojo");
		getOrdi().attack();
		notifierObservateurs();
	}

	public void move(){
		getOrdi().move();
		notifierObservateurs();
	}

	/**
	 * 
	 * @return
	 */
	public int getPV() {
		int total = 0;
		for (IComposant composant : getComposants().values()) {
			total += composant.getPv();
		}
		return total;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getPoids() {
		int total = 0;
		for (IComposant composant : getComposants().values()) {
			total += composant.getPoids();
		}
		return total;
	}

	/**
	 * 
	 * @param t
	 */
	public void attaquerCible(Tank t) {
		System.out.println("Ma cible est : "+t.getName() + " il lui reste : "+t.getPV());
		Arme arme = (Arme) getComposants().get(EComposants.ARME);
		if(this.getBatterie().getStamina()>= arme.getConsommation()){
			if(arme.getDateLastShot().getTime()+arme.getDelaisTir() < new Date().getTime()){
				arme.tirer(this,t);
				target = t;
				consommer(arme.getConsommation());
				arme.setDateLastShot(new Date());
			}
			else {
				System.out.println("je recharge ! ");
			}
			notifierObservateurs();
		}
	}
	
	/**
	 * 
	 * @param pointCible
	 */
	public void deplacer(Point2D pointCible){
		Propulsion propulsion = (Propulsion)getComposants().get(EComposants.PROPULTION);
		if(this.getBatterie().getStamina()>= propulsion.getConsommation()){
			setPosition(propulsion.deplacer(this.position,pointCible,this));
			consommer(propulsion.getConsommation());
		}
		notifierObservateurs();
	}
	
	/**
	 * 
	 * @param pointCible
	 * @return
	 */
	public Point2D simulerDeplacement(Point2D pointCible){
		Propulsion propulsion = (Propulsion)getComposants().get(EComposants.PROPULTION);
		return  propulsion.deplacer(this.position,pointCible,this);
	}
	
	/**
	 * 
	 * @param value
	 */
	public void consommer(int value) {
		System.err.println("consommation de : "+value);
		Batterie batterie = (Batterie) getComposants().get(EComposants.BATTERIE);

		batterie.consommer(value,this);
		notifierObservateurs();
	}

	public void recharger() {
		Generateur generateur = this.getGenerateur();
		Batterie batterie = this.getBatterie();

		batterie.recharger(generateur.produire(this),this);
		System.err.println("Voici la valeur de la batterie"  +batterie.getStamina());
		notifierObservateurs();
	}

	public Point2D getPosition() {
		return position;
	}

	/**
	 * 
	 * @param position
	 */
	public void setPosition(Point2D position) {
		this.position = position;
		this.setCenterX(position.getX());
		this.setCenterY(position.getY());
	}

	/**
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param gc
	 */
	public void drawTank(GraphicsContext gc){
		gc.setFill(couleur);
		gc.fillOval(position.getX()-15/2.0, position.getY()-15/2.0, 15, 15);
		if(target != null){
			gc.setStroke(Color.RED);
			gc.setLineJoin(StrokeLineJoin.MITER);
			gc.setLineWidth(3);
			gc.strokeLine(position.getX(), position.getY(), target.getPosition().getX(), target.getPosition().getY());
			ttl++;
		}
		if(ttl == 3){
			target = null;
			ttl = 0;
		}
	}

	/**
	 * 
	 * @param i
	 */
	public void recevoirDegat(int i) {
		getOrdi().defense(i);
	}

	/**
	 * 
	 * @return
	 */
	public Map<EComposants, IComposant> getComposants() {
		return composants;
	}

	/**
	 * 
	 * @param composants
	 */
	public void setComposants(Map<EComposants, IComposant> composants) {
		this.composants = composants;
	}

	/**
	 * 
	 * @return
	 */
	public Ordinateur getOrdi() {
		return ordi;
	}

	public void setOrdi(Ordinateur ordi) {
		this.ordi = ordi;
	}

	/**
	 * 
	 * @return
	 */
	public Arme getArme(){
		return (Arme) this.composants.get(EComposants.ARME);
	}

	/**
	 * 
	 * @return
	 */
	public Batterie getBatterie(){
		return (Batterie)getComposants().get(EComposants.BATTERIE);
	}

	/**
	 * 
	 * @return
	 */
	public Generateur getGenerateur(){
		return (Generateur)getComposants().get(EComposants.GENERATEUR);
	}

	/**
	 * 
	 * @return
	 */
	public Propulsion getPropulsion(){
		return (Propulsion)getComposants().get(EComposants.PROPULTION);
	}

	public enum EComposants{
		ARME, 
		BATTERIE,
		GENERATEUR, 
		PROPULTION;
	}

	@Override
	public void ajouterObservateur(TankObservateur o) {
		this.tabObservateur.add(o);
	}

	@Override
	public void notifierObservateurs() {
		for(int i=0; i<tabObservateur.size(); i++){
		        TankObservateur o = (TankObservateur) this.tabObservateur.get(i);
		        o.actualiser(this);
		}
	}

}
