package fr.unice.miage.projet.robotTurfuWar.model.v2.composant;

public interface IComposant {

	public String getNom();
	
	void setNom(String nom);
	
	public int getPv();
	
	void setPv(int pv);
	
	public double getPoids();
	
	void setPoids(double poids);
	
	public int getConsommation();
	
	void setConsommation(int conso);
}
