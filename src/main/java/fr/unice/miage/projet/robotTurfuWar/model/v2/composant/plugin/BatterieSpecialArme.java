package fr.unice.miage.projet.robotTurfuWar.model.v2.composant.plugin;

import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;
import fr.unice.miage.projet.robotTurfuWar.model.v2.annotations.AAnotationNominative;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Batterie;

public class BatterieSpecialArme extends Batterie{

	/**
	 * 
	 */
	private static final long serialVersionUID = 40225475516219366L;

	/**
	 * 
	 * @param nom
	 * @param pv
	 * @param poids
	 * @param consommation
	 * @param stamina
	 */
	@AAnotationNominative(nom = {"nom","pv","poids","consommation","stamina"})	
	public BatterieSpecialArme(String nom, int pv, double poids, int consommation, int stamina) {
		super(nom, pv, poids, consommation, stamina);
	}
	
	public BatterieSpecialArme() {
	}
	
	@Override
	public void consommer(int value,Tank me,Tank...tanks) {
		if(me.getArme().getPv() == 100){
			value= value/2;
		}
		this.setStamina(this.getStamina() - value);
	}

	@Override
	public void recharger(int value,Tank me) {
		this.setStamina(this.getStamina() + value);
	}

}
