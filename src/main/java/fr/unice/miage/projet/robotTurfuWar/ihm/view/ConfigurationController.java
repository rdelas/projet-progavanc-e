package fr.unice.miage.projet.robotTurfuWar.ihm.view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Optional;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import fr.unice.miage.projet.robotTurfuWar.ihm.model.IInfosTank;
import fr.unice.miage.projet.robotTurfuWar.ihm.model.PluginConfiguration;
import fr.unice.miage.projet.robotTurfuWar.ihm.model.PluginConfiguration.EPlugin;
import fr.unice.miage.projet.robotTurfuWar.ihm.model.PluginListConfiguration;
import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.IAttackBehavior;
import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.IDefenseBehavior;
import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.IMoveBehavior;
import fr.unice.miage.projet.robotTurfuWar.model.plugin.IhmPlugins;
import fr.unice.miage.projet.robotTurfuWar.model.plugin.OrdinateurPlugins;
import fr.unice.miage.projet.robotTurfuWar.model.plugin.TankPlugins;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Arme;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Batterie;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Generateur;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Propulsion;
import fr.unice.miage.projet.robotTurfuWar.view.launcher.Launcher;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Callback;

public class ConfigurationController {
	@FXML
	private TableView<PluginConfiguration> pluginTable;
	@FXML
	private TableColumn<PluginConfiguration, String> nameColumn;
	@FXML
	private TableColumn<PluginConfiguration, Boolean> activeColumn;
	private static final String S = File.separator;
	public static final File XMLDATA = new File("plugin"+S+"pluginsConfig.xml");
	private ObservableList<PluginConfiguration> configData = FXCollections.observableArrayList();

	
	@FXML
	private void initialize() {
		//colonne nom 
		this.nameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
		
		//colone active
		this.activeColumn.setCellValueFactory(cellData -> cellData.getValue().activeProperty());
		this.activeColumn.setCellFactory(new Callback<TableColumn<PluginConfiguration, Boolean>, TableCell<PluginConfiguration, Boolean>>() {
			public TableCell<PluginConfiguration, Boolean> call(TableColumn<PluginConfiguration, Boolean> p) {
				return new CheckBoxTableCell<PluginConfiguration, Boolean>();
			}
		});		
		
		this.pluginTable.setItems(this.configData);
		this.loadPlugins();
	}
	
	@FXML
	private void saveButton(ActionEvent event) {
		this.saveActivatePlugin(XMLDATA);
	}
	
	@FXML
	public void backToStarter(ActionEvent event) {
		try {
			InputStream input = ClassLoader.getSystemResourceAsStream("xml/starter.fxml");
			FXMLLoader fxmlLoader = new FXMLLoader();
			Parent root1 = (Parent) fxmlLoader.load(input);
			
			AnchorPane main = (AnchorPane) root1.lookup("#mainPane");
	        ImageView iv = (ImageView) root1.lookup("#background-image");
	        
	        iv.fitHeightProperty().bind(main.heightProperty());
	        iv.fitWidthProperty().bind(main.widthProperty());
			
			Launcher.getStageMain().setTitle("Robot War Turfu");
			Launcher.getStageMain().setScene(new Scene(root1));
			Launcher.getStageMain().centerOnScreen();
			Launcher.getStageMain().show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void loadPlugins() {
		OrdinateurPlugins ordiPlugins = OrdinateurPlugins.getInstance();
		TankPlugins tanksPlugins = TankPlugins.getInstance();
		IhmPlugins ihmPlugins = IhmPlugins.getInstance();
		ArrayList<PluginConfiguration> pluginActive = this.loadActivatePlugins(XMLDATA);
		
		for (Class<? extends IAttackBehavior> attack : ordiPlugins.getAttacks() ) {
			this.configData.add(new PluginConfiguration(attack.getSimpleName(), false, attack.getPackage(), EPlugin.ORDINATEUR));
		}
		
		for (Class<? extends IDefenseBehavior> defense : ordiPlugins.getDefenses() ) {
			this.configData.add(new PluginConfiguration(defense.getSimpleName(), false, defense.getPackage(), EPlugin.ORDINATEUR));
		}
		
		for (Class<? extends IMoveBehavior> attack : ordiPlugins.getMoves() ) {
			this.configData.add(new PluginConfiguration(attack.getSimpleName(), false, attack.getPackage(), EPlugin.ORDINATEUR));
		}
		
		for (Class<? extends Arme> arme : tanksPlugins.getArmes() ) {
			this.configData.add(new PluginConfiguration(arme.getSimpleName(), false, arme.getPackage(), EPlugin.TANK));
		}
		
		for (Class<? extends Batterie> batterie : tanksPlugins.getBatteries() ) {
			this.configData.add(new PluginConfiguration(batterie.getSimpleName(), false, batterie.getPackage(), EPlugin.TANK));
		}
			
		for (Class<? extends Generateur> generateur : tanksPlugins.getGenerateurs() ) {
			this.configData.add(new PluginConfiguration(generateur.getSimpleName(), false, generateur.getPackage(), EPlugin.TANK));
		}
		
		for (Class<? extends Propulsion> propulsion : tanksPlugins.getPropulsions() ) {
			this.configData.add(new PluginConfiguration(propulsion.getSimpleName(), false, propulsion.getPackage(), EPlugin.TANK));
		}
		
	    for (Class<? extends IInfosTank> ihm : ihmPlugins.getInfosTank()) {
	        this.configData.add(new PluginConfiguration(ihm.getSimpleName(), false, ihm.getPackage(), EPlugin.GRAPHIQUE));
	    }
		
		for (PluginConfiguration plugin : configData) {
			for (PluginConfiguration p : pluginActive) {
				if(plugin.getName().equals(p.getName())) {
					plugin.setActive(true);
				}
			}
		}	
	}
	
	/**
	 * 
	 * @param file
	 * @return
	 */
	public ArrayList<PluginConfiguration> loadActivatePlugins(File file) {
		ArrayList<PluginConfiguration> pluginActive = new ArrayList<>();
		try {
			JAXBContext context = JAXBContext.newInstance(PluginListConfiguration.class);
			Unmarshaller um = context.createUnmarshaller();

			PluginListConfiguration wrapper = (PluginListConfiguration) um.unmarshal(file);
			pluginActive.addAll(wrapper.getPlugins());
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Could not load data");
			alert.setContentText("Could not load data from file:\n" + file.getPath());
			alert.showAndWait();
			e.printStackTrace();
		}
		
		return pluginActive;
	}

	/**
	 * 
	 * @param file
	 */
	public void saveActivatePlugin(File file) {
		try {
			JAXBContext context = JAXBContext.newInstance(PluginListConfiguration.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			PluginListConfiguration wrapper = new PluginListConfiguration();
			wrapper.setPlugins(this.getActivePlugins());

			m.marshal(wrapper, file);
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Could not save data");
			alert.setContentText("Could not save data to file:\n" + file.getPath());
			alert.showAndWait();
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public ArrayList<PluginConfiguration> getActivePlugins() {
		ArrayList<PluginConfiguration> pluginActive = new ArrayList<>();
		
		for (PluginConfiguration plugin : this.configData) {
			if(plugin.getActive()) {
				pluginActive.add(plugin);
			}
		}	
		return pluginActive;
	}	
	

	/**
	 * M�thode permettant l'ajout de Plugin avec Choose File
	 * @param event
	 */
	public void addPlugin(ActionEvent event){
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Importer un plugin");
		alert.setHeaderText("Choisissez le type de plugin � importer.");

		ButtonType btnOrdi = new ButtonType("Ordinateur");
		ButtonType btnTank = new ButtonType("Tank");
		ButtonType btnGraph = new ButtonType("Graphique");
		ButtonType btnCancel = new ButtonType("Annuler", ButtonData.CANCEL_CLOSE);

		alert.getButtonTypes().setAll(btnOrdi, btnTank, btnGraph, btnCancel);

		Optional<ButtonType> result = alert.showAndWait();
		
		if(result.get() != btnCancel) {
			String path = null;
			if(result.get() == btnOrdi) {
				path = "plugin"+S+EPlugin.ORDINATEUR.getValue();
			} else if (result.get() == btnTank) {
				path = "plugin"+S+EPlugin.TANK.getValue();
			} else if (result.get() == btnGraph) {
				path = "plugin"+S+EPlugin.GRAPHIQUE.getValue();
			}

			try{
				File directory = new File(path);
				FileChooser fileChooser = new FileChooser();
				fileChooser.setTitle("Open Resource File");
				fileChooser.getExtensionFilters().addAll(
				         new ExtensionFilter("Class Java", "*.class"),
				         new ExtensionFilter("Jar Files", "*.jar"));
				File selectedFile = fileChooser.showOpenDialog(Launcher.getStageMain());
	
				if (selectedFile != null) {
					String fileName = selectedFile.getName();
					
					if(fileName.endsWith(".class")) {			
						String ligne;
						String packageName = "";
						BufferedReader br = new BufferedReader(new FileReader(selectedFile));
						while((ligne = br.readLine()) != null) {
							if(ligne.contains("����")) {
								byte[] b = ligne.substring(16).getBytes();
								for (int i = 0; i < b.length; i++) {
									if((char)b[i] == 0 || (char)b[i] == 7) {
										break;
									}
									packageName += (char)b[i];
								}
								break;
							}
						}
						br.close();
						
						String name = fileName.substring(0, fileName.lastIndexOf(".class"));
						packageName = packageName.substring(0, packageName.lastIndexOf(name)-1);					
						directory = new File(directory+S+packageName);
						directory.mkdirs();
					}
					
					boolean success = selectedFile.renameTo(new File(directory, fileName));
					
					if(success){
						this.configData.clear();
						OrdinateurPlugins.reload();
						TankPlugins.reload();
						IhmPlugins.reload();
						this.pluginTable.setItems(this.configData);
						this.loadPlugins();
					}
				}	
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 *  Methode pour supprimer les plugin
	 */
	@FXML
	private void deletePlugin() {		
		ObservableList<PluginConfiguration> pluginSelected, allPlugin;
		allPlugin  = pluginTable.getItems();
		pluginSelected = pluginTable.getSelectionModel().getSelectedItems();

		System.out.println("Le nom du plugin a supprimer est " + pluginTable.getSelectionModel().getSelectedItem().getName());
		
		try {
			String directory = "plugin"+S+pluginTable.getSelectionModel().getSelectedItem().getType().getValue()+S;
			String packageName = pluginTable.getSelectionModel().getSelectedItem().getPackage().getName().replace(".", S)+S;
			System.out.println(packageName);
			String className = pluginTable.getSelectionModel().getSelectedItem().getName()+".class";
			
			Files.delete((Paths.get(directory+packageName+className)));
			pluginSelected.forEach(allPlugin :: remove);
		} catch (NoSuchFileException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Impossible de supprimer le fichier");
			alert.setContentText("Impossible de supprimer un fichier qui se trouve dans un jar.");
			alert.showAndWait();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}