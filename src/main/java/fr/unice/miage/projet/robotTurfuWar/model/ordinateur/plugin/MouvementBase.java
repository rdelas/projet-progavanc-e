package fr.unice.miage.projet.robotTurfuWar.model.ordinateur.plugin;

import java.util.List;

import fr.unice.miage.projet.robotTurfuWar.model.ChampDeTank;
import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.IMoveBehavior;
import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;
import javafx.geometry.Point2D;

public class MouvementBase implements IMoveBehavior{
	
	@Override
	public void action(Tank me) {
		ChampDeTank champ = ChampDeTank.getChamp();
		List<Tank> list = champ.getTanks();
		Tank plusProche = getNearestTank(me, list);
		if(plusProche != null){

			Point2D temp = me.simulerDeplacement(plusProche.getPosition());

			if(!plusProche.contains(temp)){
				me.deplacer(plusProche.getPosition());
			}
		} else {
			MouvementAleatoire m = new MouvementAleatoire();
			me.getOrdi().setMoveBehavior(m);
			m.action(me);
		}

	}

}
