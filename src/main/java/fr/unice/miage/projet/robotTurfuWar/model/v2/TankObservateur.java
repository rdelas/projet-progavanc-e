package fr.unice.miage.projet.robotTurfuWar.model.v2;

public interface TankObservateur {
	public void actualiser(TankObservable o);
}
