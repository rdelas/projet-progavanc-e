package fr.unice.miage.projet.robotTurfuWar.model.ordinateur;

import java.util.List;

import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;

public interface IMoveBehavior extends IBehavior{

	public void action(Tank me);
	
	default Tank getNearestTank(Tank me, List<Tank> list){
		Tank plusProche = null;
		for(Tank t : list){
			if(t.equals(me))
				continue;
			
			if(t.getPV()>0){
				if(plusProche==null){
					plusProche=t;
				}
				if(t.getPosition().distance(me.getPosition())< plusProche.getPosition().distance(me.getPosition())){
					plusProche=t;
				}
			}

		}
		return plusProche;
	}
}
