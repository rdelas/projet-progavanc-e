package fr.unice.miage.projet.robotTurfuWar.model.plugin;

import java.io.File;
import java.util.List;

import com.unice.miage.master.repository.RepositoryT;

import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.IAttackBehavior;
import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.IDefenseBehavior;
import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.IMoveBehavior;

public class OrdinateurPlugins {

	private static OrdinateurPlugins instance;
	private static final File BASE = new File("plugin" + File.separator + "ordi"+File.separator);
	private List<Class<? extends IAttackBehavior>> attacks;
	private List<Class<? extends IMoveBehavior>> moves;
	private List<Class<? extends IDefenseBehavior>> defenses;
	private OrdinateurPlugins() {
		loadAttacks();
		loadMoves();
		loadDefenses();
	}

	/**
	 * 
	 * @return
	 */
	public static OrdinateurPlugins getInstance() {
		if (instance == null) {
			instance = new OrdinateurPlugins();
		}
		return instance;
	}

	public static void reload() {
		instance.loadAttacks();
		instance.loadMoves();
		instance.loadDefenses();
	}
	
	private void loadAttacks() {
		RepositoryT<IAttackBehavior> rAttacks = new RepositoryT<IAttackBehavior>(BASE, IAttackBehavior.class);
		attacks = rAttacks.load();
	}
	
	private void loadMoves() {
		RepositoryT<IMoveBehavior> rMoves = new RepositoryT<IMoveBehavior>(BASE, IMoveBehavior.class);
		moves = rMoves.load();
	}
	
	private void loadDefenses() {
		RepositoryT<IDefenseBehavior> rDefenses = new RepositoryT<IDefenseBehavior>(BASE, IDefenseBehavior.class);
		defenses = rDefenses.load();
	}
	/**
	 * 
	 * @return
	 */
	public List<Class<? extends IAttackBehavior>> getAttacks() {
		return attacks;
	}

	/**
	 * 
	 * @return
	 */
	public List<Class<? extends IMoveBehavior>> getMoves() {
		return moves;
	}

	/**
	 * 
	 * @return
	 */
	public List<Class<? extends IDefenseBehavior>> getDefenses() {
		return defenses;
	}
}
