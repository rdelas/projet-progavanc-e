package fr.unice.miage.projet.robotTurfuWar.model.v2.composant.plugin;

import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;
import fr.unice.miage.projet.robotTurfuWar.model.v2.annotations.AAnotationNominative;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Generateur;

public class GenerateurBase extends Generateur {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7851845597836344526L;

	public GenerateurBase() {
	}
	/**
	 * 
	 * @param nom
	 * @param pv
	 * @param poids
	 * @param consommation
	 * @param production
	 * @param ressource
	 */
	@AAnotationNominative(nom = {"nom","pv","poids","consommation","production","ressource"})
	public GenerateurBase(String nom, int pv, double poids, int consommation, int production, int ressource) {
		super(nom, pv, poids, consommation, production, ressource);
	}

	@Override
	public int produire(Tank me) {
		if(ressource>0){
			consommerRessource();
			return this.getProduction();
		} else{
			System.out.println("Pane de ressource");
			return 0;
		}
	}

	@Override
	public void consommerRessource() {
		this.ressource -= this.consommation;

	}

}
