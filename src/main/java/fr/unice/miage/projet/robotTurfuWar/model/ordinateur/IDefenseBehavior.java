package fr.unice.miage.projet.robotTurfuWar.model.ordinateur;

import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;

public interface IDefenseBehavior extends IBehavior{


	void action(Tank tank, int degats);

}
