package fr.unice.miage.projet.robotTurfuWar.model.ordinateur.plugin;

import java.util.Map;

import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.IDefenseBehavior;
import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;
import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank.EComposants;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.IComposant;

public class DefenseClassic implements IDefenseBehavior{

	@Override
	public void action(Tank tank, int degats) {
		Map<EComposants, IComposant> listcomp = tank.getComposants();
		int nbCompo = listcomp.size();
		for(IComposant compo : listcomp.values()){
			compo.setPv(compo.getPv()-(degats/nbCompo));
		}
	}
}
