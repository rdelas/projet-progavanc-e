package fr.unice.miage.projet.robotTurfuWar.model.ordinateur;

import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;

public interface IAttackBehavior extends IBehavior{

	public void action(Tank me);
	
}
