package fr.unice.miage.projet.robotTurfuWar.model.v2.composant;

import java.io.Serializable;

import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;

public abstract class Batterie implements IComposant,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1012848392472549431L;
	protected String nom;
	protected int pv;
	protected double poids;
	private int consommation;
	private int stamina;

	/**
	 * 
	 * @param nom
	 * @param pv
	 * @param poids
	 * @param consommation
	 * @param stamina
	 */
	public Batterie(String nom, int pv, double poids, int consommation, int stamina) {
		super();
		this.nom = nom;
		this.pv = pv;
		this.poids = poids;
		this.setConsommation(consommation);
		this.setStamina(stamina);
	}
	
	public Batterie() {
	}
	
	@Override
	public String getNom() {
		return nom;
	}

	
	@Override
	public void setNom(String nom) {
		this.nom=nom;
	}

	@Override
	public int getPv() {
		return pv;
	}

	@Override
	public void setPv(int pv) {
		this.pv=pv;
	}

	@Override
	public double getPoids() {
		return this.poids;
	}

	@Override
	public void setPoids(double poids) {
		this.poids=poids;
	}

	@Override
	public int getConsommation() {
		return this.consommation;
	}

	@Override
	public void setConsommation(int conso) {
		this.consommation = conso;

	}
	
	public void setStamina(int stamina) {
		this.stamina = stamina;
	}
	
	public int getStamina() {
		return this.stamina;
	}

	/**
	 * 
	 * @param value
	 * @param me
	 * @param tanks
	 */
	public abstract void consommer(int value,Tank me, Tank...tanks);

	/**
	 * 
	 * @param value
	 * @param me
	 */
	public  abstract void recharger(int value,Tank me);




}
