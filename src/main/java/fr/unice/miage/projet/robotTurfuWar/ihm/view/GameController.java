package fr.unice.miage.projet.robotTurfuWar.ihm.view;

import java.io.InputStream;

import fr.unice.miage.projet.robotTurfuWar.model.ChampDeTank;
import fr.unice.miage.projet.robotTurfuWar.view.launcher.Launcher;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class GameController {	
	@FXML
	private AnchorPane champ;
	
	@FXML
	private AnchorPane plugin;
	
	/**
	 * Methode permettant de revenir au d�marrage du jeu
	 * @param event
	 */
	@FXML
	public void backToStarter(ActionEvent event) {
		try {
			ChampDeTank champ = ChampDeTank.getChamp();
			champ.stop();
			InputStream input = ClassLoader.getSystemResourceAsStream("xml/starter.fxml");
			FXMLLoader fxmlLoader = new FXMLLoader();
			Parent root1 = (Parent) fxmlLoader.load(input);
			
			AnchorPane main = (AnchorPane) root1.lookup("#mainPane");
	        ImageView iv = (ImageView) root1.lookup("#background-image");
	        
	        iv.fitHeightProperty().bind(main.heightProperty());
	        iv.fitWidthProperty().bind(main.widthProperty());
			
			Launcher.getStageMain().setTitle("Robot War Turfu");
			Launcher.getStageMain().setScene(new Scene(root1));
			Launcher.getStageMain().centerOnScreen();
			Launcher.getStageMain().show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
