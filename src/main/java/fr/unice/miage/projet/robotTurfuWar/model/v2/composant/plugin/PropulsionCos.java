package fr.unice.miage.projet.robotTurfuWar.model.v2.composant.plugin;

import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;
import fr.unice.miage.projet.robotTurfuWar.model.v2.annotations.AAnotationNominative;
import fr.unice.miage.projet.robotTurfuWar.model.v2.composant.Propulsion;
import javafx.geometry.Point2D;

public class PropulsionCos extends Propulsion {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7678149324646114580L;

	public PropulsionCos() {
	}
	
	/**
	 * 
	 * @param nom
	 * @param pv
	 * @param poids
	 * @param consommation
	 * @param deplacement
	 */
	@AAnotationNominative(nom = {"nom","pv","poids","consommation","deplacement"})
	public PropulsionCos(String nom, int pv, double poids, int consommation, double deplacement) {
		super(nom, pv, poids, consommation, deplacement);
	}

	@Override
	public Point2D deplacer(Point2D maPos, Point2D posCible,Tank me) {
		double x = maPos.getX();
		double y = maPos.getY();
		double deplacementX =posCible.getX() -maPos.getX();
		double deplacementY = posCible.getY() -maPos.getY();
		double direction = Math.atan2(deplacementY, deplacementX); // Math.atan2(deltaY, deltaX) does the same thing but checks for deltaX being zero to prevent divide-by-zero exceptions
		double speed = this.getDeplacement();
		
		x+=  (speed * Math.cos(direction));
		y+=  (speed * Math.sin(direction));
		return new Point2D(x, y);

	}
}