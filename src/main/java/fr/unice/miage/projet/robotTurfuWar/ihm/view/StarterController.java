package fr.unice.miage.projet.robotTurfuWar.ihm.view;

import java.io.InputStream;

import fr.unice.miage.projet.robotTurfuWar.view.launcher.Launcher;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;


public class StarterController {
	
	@FXML
	private AnchorPane zoneChamp;
	
	@FXML
	private AnchorPane zoneInfo; 
	
	/**
	 * Methode permettant d'acceder aux configurations des plugins
	 * @param event
	 */
	@FXML
	public void pressButton(ActionEvent event) {
		try {
			InputStream input = ClassLoader.getSystemResourceAsStream("xml/config.fxml");
	        FXMLLoader loader = new FXMLLoader();
			Parent root = loader.load(input);
			Scene scene = new Scene(root);
			
			Launcher.getStageMain().setTitle("Configuration des Plugins : Robot War Turfu");
	        Launcher.getStageMain().setScene(scene);
			Launcher.getStageMain().centerOnScreen();
	        Launcher.getStageMain().show();

			ConfigurationController controller = loader.getController();
			controller.loadActivatePlugins(ConfigurationController.XMLDATA);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Methode permettant d'acceder aux configurations des tank pour lancer une partie
	 * @param event
	 */
	@FXML
	public void openStart(ActionEvent event) {
		try {
			TankSelectorController t = new TankSelectorController();
			t.start();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
