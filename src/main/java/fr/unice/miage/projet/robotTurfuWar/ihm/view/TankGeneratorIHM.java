package fr.unice.miage.projet.robotTurfuWar.ihm.view;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.IAttackBehavior;
import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.IDefenseBehavior;
import fr.unice.miage.projet.robotTurfuWar.model.ordinateur.IMoveBehavior;
import fr.unice.miage.projet.robotTurfuWar.model.plugin.OrdinateurPlugins;
import javafx.collections.FXCollections;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.GridPane;
import javafx.util.StringConverter;

public class TankGeneratorIHM extends GridPane {

	private static final String S = File.separator;

	private static final String BASE = "xml" + S + "composants" + S;

	public TankGeneratorIHM() {
	}

	public void loadAll() {

		loadArmes();
		loadBatteries();
		loadGenerateurs();
		loadPropulsions();

		loadAttacks();
		loadMoves();
		loadDefenses();
	}

	@SuppressWarnings("unchecked")
	private void loadArmes() {
		File f = new File(BASE);
		List<File> files = listFilteredFolderRecursive(f, ".*\\.Arme");
		ChoiceBox<File> cb = ((ChoiceBox<File>) lookup("#armeChoice"));
		cb.setConverter(new StringConverter<File>() {

			@Override
			public String toString(File object) {
				return object.getName().replaceFirst("[.][^.]+$", "");
			}

			@Override
			public File fromString(String string) {
				return null;
			}
		});
		cb.setItems(FXCollections.observableArrayList(files));
		cb.getSelectionModel().selectFirst();
	}

	@SuppressWarnings("unchecked")
	private void loadBatteries() {
		File f = new File(BASE);
		List<File> files = listFilteredFolderRecursive(f, ".*\\.Batterie");
		ChoiceBox<File> cb = ((ChoiceBox<File>) lookup("#batterieChoice"));
		cb.setConverter(new StringConverter<File>() {

			@Override
			public String toString(File object) {
				return object.getName().replaceFirst("[.][^.]+$", "");
			}

			@Override
			public File fromString(String string) {
				// TODO Auto-generated method stub
				return null;
			}
		});
		cb.setItems(FXCollections.observableArrayList(files));
		cb.getSelectionModel().selectFirst();
	}

	@SuppressWarnings("unchecked")
	private void loadGenerateurs() {
		File f = new File(BASE);
		List<File> files = listFilteredFolderRecursive(f, ".*\\.Generateur");
		ChoiceBox<File> cb = ((ChoiceBox<File>) lookup("#generateurChoice"));
		cb.setConverter(new StringConverter<File>() {

			@Override
			public String toString(File object) {
				return object.getName().replaceFirst("[.][^.]+$", "");
			}

			@Override
			public File fromString(String string) {
				// TODO Auto-generated method stub
				return null;
			}
		});
		cb.setItems(FXCollections.observableArrayList(files));
		cb.getSelectionModel().selectFirst();
	}

	@SuppressWarnings("unchecked")
	private void loadPropulsions() {
		File f = new File(BASE);
		List<File> files = listFilteredFolderRecursive(f, ".*\\.Propulsion");

		ChoiceBox<File> cb = ((ChoiceBox<File>) lookup("#propulsionChoice"));
		cb.setConverter(new StringConverter<File>() {

			@Override
			public String toString(File object) {
				return object.getName().replaceFirst("[.][^.]+$", "");
			}

			@Override
			public File fromString(String string) {
				return null;
			}
		});
		cb.setItems(FXCollections.observableArrayList(files));
		cb.getSelectionModel().selectFirst();
	}

	@SuppressWarnings("unchecked")
	private void loadAttacks() {
		List<Class<? extends IAttackBehavior>> attacks = OrdinateurPlugins.getInstance().getAttacks();
		ChoiceBox<Class<? extends IAttackBehavior>> cb = ((ChoiceBox<Class<? extends IAttackBehavior>>) lookup(
				"#attackChoice"));
		cb.setConverter(new StringConverter<Class<? extends IAttackBehavior>>() {

			@Override
			public String toString(Class<? extends IAttackBehavior> object) {
				return object.getSimpleName();
			}

			@Override
			public Class<? extends IAttackBehavior> fromString(String string) {
				return null;
			}
		});
		cb.setItems(FXCollections.observableArrayList(attacks));
		cb.getSelectionModel().selectFirst();
	}

	@SuppressWarnings("unchecked")
	private void loadMoves() {
		List<Class<? extends IMoveBehavior>> moves = OrdinateurPlugins.getInstance().getMoves();
		ChoiceBox<Class<? extends IMoveBehavior>> cb = ((ChoiceBox<Class<? extends IMoveBehavior>>) lookup(
				"#moveChoice"));
		cb.setConverter(new StringConverter<Class<? extends IMoveBehavior>>() {

			@Override
			public String toString(Class<? extends IMoveBehavior> object) {
				return object.getSimpleName();
			}

			@Override
			public Class<? extends IMoveBehavior> fromString(String string) {
				return null;
			}
		});
		cb.setItems(FXCollections.observableArrayList(moves));
		cb.getSelectionModel().selectFirst();
	}

	@SuppressWarnings("unchecked")
	private void loadDefenses() {
		List<Class<? extends IDefenseBehavior>> defenses = OrdinateurPlugins.getInstance().getDefenses();
		ChoiceBox<Class<? extends IDefenseBehavior>> cb = ((ChoiceBox<Class<? extends IDefenseBehavior>>) lookup(
				"#defenseChoice"));
		cb.setConverter(new StringConverter<Class<? extends IDefenseBehavior>>() {

			@Override
			public String toString(Class<? extends IDefenseBehavior> object) {
				return object.getSimpleName();
			}

			@Override
			public Class<? extends IDefenseBehavior> fromString(String string) {
				return null;
			}
		});
		cb.setItems(FXCollections.observableArrayList(defenses));
		cb.getSelectionModel().selectFirst();
	}

	/**
	 * 
	 * @param file
	 * @param filter
	 * @return
	 */
	private static List<File> listFilteredFolderRecursive(File file, String filter) {
		List<File> files = new ArrayList<File>();

		FilenameFilter filterObj = new FilenameFilterRecursive(filter);
		File[] listFiles = file.listFiles(filterObj);

		for (File subFile : listFiles) {

			if (subFile.isDirectory()) {
				files.addAll(listFilteredFolderRecursive(subFile, filter));
			} else {
				files.add(subFile);
			}
		}
		return files;
	}

	static class FilenameFilterRecursive implements FilenameFilter {

		private String regex;

		public FilenameFilterRecursive(String regex) {
			this.regex = regex;
		}

		public boolean accept(File dir, String name) {
			File f = new File(dir.getPath() + "/" + name);
			if (f.isDirectory()) {
				return true;
			}

			Pattern p = Pattern.compile(regex);

			Matcher m = p.matcher(name);
			return m.matches();
		}

	}
}
