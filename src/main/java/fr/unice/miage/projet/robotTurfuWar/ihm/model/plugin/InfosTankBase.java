package fr.unice.miage.projet.robotTurfuWar.ihm.model.plugin;

import fr.unice.miage.projet.robotTurfuWar.ihm.model.IInfosTank;
import fr.unice.miage.projet.robotTurfuWar.model.ChampDeTank;
import fr.unice.miage.projet.robotTurfuWar.model.v2.Tank;
import fr.unice.miage.projet.robotTurfuWar.model.v2.TankObservable;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class InfosTankBase implements IInfosTank {
	private VBox vbox;
	
	@Override
	public void getInfosTank(Pane zoneInfo) {
		ChampDeTank champ = ChampDeTank.getChamp();
		this.vbox = new VBox();
		this.vbox.setSpacing(15);
		this.vbox.setStyle("-fx-background-color: darkseagreen;");
		
		for (Tank tank : champ.getTanks()) {
			Pane infoPane = new Pane();

			infoPane.setStyle("-fx-background-color: lightgray;");
			infoPane.setPrefSize(300,110);
		
			Label nom = new Label("NOM :");
			nom.setTranslateX(10);
			nom.setTranslateY(30);
			nom.setTextFill(tank.getFill());
			nom.setFont(Font.font("Verdana", FontWeight.BOLD, 12));
			
			Label nomValue = new Label(tank.getName());
			nomValue.setTranslateX(110);
			nomValue.setTranslateY(30);
			
			Label pv = new Label("PV :");
			pv.setTranslateX(10);
			pv.setTranslateY(50);
			pv.setFont(Font.font("Verdana", FontWeight.BOLD, 12));
			
			Label pvValue = new Label(tank.getPV()+"");
			pvValue.setTranslateX(110);
			pvValue.setTranslateY(50);
			pvValue.setId("PV-"+tank.hashCode());
			
			Label energie = new Label("ENERGIE ");
			energie.setTranslateX(10);
			energie.setTranslateY(70);
			energie.setFont(Font.font("Verdana", FontWeight.BOLD, 12));
			
			Label energieValue = new Label(tank.getBatterie().getStamina()+"");
			energieValue.setTranslateX(110);
			energieValue.setTranslateY(70);
			energieValue.setId("Energie-"+tank.hashCode());
			
			infoPane.getChildren().addAll(nom, nomValue, pv, pvValue, energie, energieValue);
			
			
			this.vbox.getChildren().addAll(infoPane);
			tank.ajouterObservateur(this);
		
		}
		ScrollPane spane = new ScrollPane();
		spane.setVmin(this.vbox.getHeight());
		spane.setPrefSize(zoneInfo.getWidth(), zoneInfo.getHeight());
		spane.setStyle("-fx-background-color: darkseagreen;");
		spane.setContent(this.vbox);
		zoneInfo.getChildren().add(spane);
	}

	@Override
	public void actualiser(TankObservable o) {
		if(o instanceof Tank) {
			Tank tank = (Tank) o;
			
			int tpv = tank.getPV();
			if(tpv < 0) {
				tpv = 0;
			}
			
			final int pv = tpv;
			int energie = tank.getBatterie().getStamina();
			
			Platform.runLater(() -> ((Labeled) this.vbox.lookup("#PV-"+tank.hashCode())).setText(""+pv));
			Platform.runLater(() -> ((Labeled) this.vbox.lookup("#Energie-"+tank.hashCode())).setText(""+energie));
		}
	}
}
