package fr.unice.miage.projet.robotTurfuWar.model.v2;

import java.util.ArrayList;
import java.util.List;

public class TankConfigurations {

	private static final TankConfigurations INSTANCE = new TankConfigurations();
	private List<TankConfig> configs = new ArrayList<TankConfig>();
	
	/**
	 * 
	 * @return
	 */
	public static TankConfigurations getInstance(){
		return INSTANCE;
	}
	
	/**
	 * 
	 * @param config
	 */
	public void addConfig(TankConfig config){
		configs.add(config);
	}
	
	/**
	 * 
	 * @return
	 */
	public List<TankConfig> getConfigs(){
		return configs;
	}
	
	public void resetConfig() {
		configs.clear();
	}
}
