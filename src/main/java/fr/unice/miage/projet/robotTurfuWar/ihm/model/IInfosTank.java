package fr.unice.miage.projet.robotTurfuWar.ihm.model;

import fr.unice.miage.projet.robotTurfuWar.model.v2.TankObservateur;
import javafx.scene.layout.Pane;

public interface IInfosTank extends TankObservateur {
	public void getInfosTank(Pane zoneInfo);
}